# My Thermcam Demo

## Installation

Open the project with Android Studio

## sdk path

app/lib/thermcamsdk.aar

## Usage

- initial
    ```java
    private InfraredThermometer cam;
    cam = new InfraredThermometer.Builder(this)
                    .setHardwareResolution(LEP_W, LEP_H)
                    .setScale(SCALE)
                    .buildAsClient("192.168.43.1", 2020);
    ```
	
- set ImageView callback
  ```java
  private ImageView imgLepton;
  imgLepton = (ImageView) findViewById(R.id.imgLepton);
  cam.setOnLeptonBitmapListener(new OnBitmapListener() {
    @Override
    public void onReceived(final Bitmap bitmap) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imgLepton.setImageBitmap(bitmap);
            }
        });
    }
  });
  ```
  
- set temperature information callback
  ```java
  cam.setOnInfoListener(new OnInfoListener() {
    @Override
    public void onInfo(final short status, String message) {
        Log.i(TAG, "stat: " + status + "m: " + message);
    }
  });
  ```
  
- start thermal camera
  ```java
  cam.start();
  ```
  
- stop thermal camera
  ```java
  cam.stop();
  ```
  
## API GUIDE

| API | README |
| ------ | ------ |
| void start(); | start the thermal camera. |
| void stop(); | stop the thermal camera. |
| String getSDKVersion(); | get SDK verion. |
| void setOnLeptonBitmapListener(OnBitmapListener l); | thermal bitmap callback. |
| void setOnInfoListener(OnInfoListener l); | thermal information callback. |
| void setBlackBodyForRealtimeCalibration (int lepX, int lepY, float Temperature); | set black body location. |
| void setBlackBodyAndMagneticForRealtimeCalibration(int lepX, int lepY, float Temperature); | set black body location and auto magnetic to hot area. | 