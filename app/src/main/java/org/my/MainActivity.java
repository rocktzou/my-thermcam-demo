package org.my;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import org.rock.thermcamsdk.OnBitmapListener;
import org.rock.thermcamsdk.OnInfoListener;
import org.rock.thermcamsdk.InfraredThermometer;
import org.rock.thermcamsdk.Sight;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private final static int DEFAULT_BB_T = 37; //Default Black Body Temperature
    public static final short LEP_W = 80, LEP_H = 60; //Hardware Resolution(width and height)
    public static final int SCALE = 11;
    public static final short VIEW_W = LEP_W * SCALE;
    public static final short VIEW_H = LEP_H * SCALE; //layout Resolution(width and height)

    public static float bbTemperature = DEFAULT_BB_T; //Black Body Temperature
    public int bbLepX = 0;
    public int bbLepY = 0;
    public int bbViewX = bbLepX * SCALE, bbViewY = bbLepY * SCALE;

    public final int NEAR = 7 * SCALE;
    private boolean actionBlackBody = false;
    private InfraredThermometer cam = null;
    private ImageView imgLepton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cam = new InfraredThermometer.Builder(this)
                .setHardwareResolution(LEP_W, LEP_H)
                .setScale(SCALE)
                .buildAsClient("192.168.43.1", 2020);

        cam.setBlackBodyForRealtimeCalibration(bbLepX, bbLepY, bbTemperature);

        imgLepton = (ImageView) findViewById(R.id.imgLepton);
        imgLepton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return onTouch_2(v, event);
            }
        });
    }

    private synchronized void startThermal() {
        cam.setOnLeptonBitmapListener(new OnBitmapListener() {
            @Override
            public void onReceived(final Bitmap bitmap) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imgLepton.setImageBitmap(bitmap);
                    }
                });
            }
        });
        cam.setOnInfoListener(new OnInfoListener() {
            @Override
            public void onInfo(final short status, String message) {
                Log.e(TAG, "stat: " + status + "m: " + message);
            }
        });
        cam.start();
    }

    private synchronized void stopThermal() {
        cam.stop();
    }

    @Override
    protected void onPause() {
        Log.e(TAG, "onPause");
        stopThermal();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
        startThermal();
    }

    private boolean onTouch_2(View v, MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        if(x>= VIEW_W || y>= VIEW_H)
            return false;
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            //Log.e(TAG, "ACTION_DOWN x:" + x + " y:" + y + "scale:" + scale);
            if(x<(bbViewX + NEAR) && x>(bbViewX - NEAR) && y<(bbViewY + NEAR) && y>(bbViewY - NEAR))
                actionBlackBody = true;
            if(actionBlackBody) {
                bbViewX = (short)x;
                bbViewY = (short)y;
                cam.setBlackBodyForRealtimeCalibration((short)(x/ SCALE), (short)(y/ SCALE), bbTemperature);
            }
        }
        else if(event.getAction() == MotionEvent.ACTION_MOVE) {
            //Log.e(TAG, "ACTION_MOVE");
            if(actionBlackBody) {
                bbViewX = (short)x;
                bbViewY = (short)y;
                cam.setBlackBodyForRealtimeCalibration((short)(x/ SCALE), (short)(y/ SCALE), bbTemperature);
            }
        }
        else if(event.getAction() == MotionEvent.ACTION_UP) {
            //Log.e(TAG, "ACTION_UP");
            if(actionBlackBody) {
                bbViewX = (short)x;
                bbViewY = (short)y;
                cam.setBlackBodyForRealtimeCalibration((short)(x/ SCALE), (short)(y/ SCALE), bbTemperature);
            }
            actionBlackBody = false;
        }
        return true;
    }
}
